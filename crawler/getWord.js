const { Word } = require('./DB')
const T = Word

async function getId(key) {
    const res = await T.findOne({ where: { Word: key } })
    if (process.argv[2] == 'getId') console.log(res.dataValues)
    return res
}
async function getWord(key) {
    const res = await T.findByPk(key)
    if (process.argv[2] == 'getWord') console.log(res.dataValues)
    return res
}
async function count() {
    const res = await T.count()
    if (process.argv[2] == 'count') console.log(res)
    return res
}
async function maxId() {
    const res = await T.max('id')
    return res
}
async function update(data) {
    const arr = []
    for (const item of data) {
        if (item.length > 255) continue // too long the word piece
        arr.push({ Word: item })
    }
    const res = await Word.bulkCreate(arr, {
        fields: ['Word'],
        updateOnDuplicate: ['Word']
    })
    return res
}

if (process.argv[1].includes('getWord')) {
    if (process.argv[2] == 'getId') getId(process.argv[3])
    if (process.argv[2] == 'getWord') getWord(process.argv[3])
    if (process.argv[2] == 'count') count()
}
module.exports = { getId, getWord, count, maxId, update }
