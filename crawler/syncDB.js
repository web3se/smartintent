/*
 * sync databases, from one to another
 */

const { sleep } = require('./utils')
const { model, mysql, sqlite } = require('./initDB')

const M1 = model(mysql(require('../config/mysql_remote.json'))) // from
const M2 = model(mysql(require('../config/mysql.json'))) // to

const from = {
    contract: M1.Contract,
    token: M1.Token,
    code: M1.Code
}
const to = {
    contract: M2.Contract,
    token: M2.Token,
    code: M2.Code
}

async function sync2mysql(name, start = 1) {
    name = name.toLowerCase()
    const max = await from[name].max('id')
    console.log('Sync', name)
    console.log('MaxId', max)
    console.log('Start', start)
    await sleep(3000)

    for (let i = start; i <= max; i++) {
        const data = await from[name].findByPk(i)
        if (!data) continue
        data.Id = null
        const row = await to[name].upsert(data.dataValues)
        console.log('Upsert to MySQL', row[0].Id)
    }

    // compare
    let count = await from[name].count()
    let maxId = await from[name].max('Id')
    console.log('from', { count, maxId })

    count = await to[name].count()
    maxId = await to[name].max('Id')
    console.log('to', { count, maxId })
}

if (process.argv[2] === 'mysql') sync2mysql(process.argv[3], process.argv[4])
