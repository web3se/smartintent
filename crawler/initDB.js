const ROOT = require('app-root-path')
const { Sequelize } = require('@sequelize/core')
const Model = require('./Model')

function sqlite() {
    return new Sequelize({
        dialect: 'sqlite',
        storage: `${ROOT}/db/db.sqlite`
    })
}

function mysql(conf, connectTimeout = 60000) {
    return new Sequelize(conf.database, conf.username, conf.password, {
        host: conf.host,
        dialectOptions: { connectTimeout },
        dialect: 'mysql'
    })
}

// test connection to db
async function connect(db = 'sqlite') {
    console.log('Connect to', db)
    let sequelize
    if (db === 'sqlite') sequelize = sqlite()
    if (db === 'mysql') sequelize = mysql(require('../config/mysql.json'))
    await sequelize.authenticate()
    console.log('Connection has been established successfully.')
    return sequelize
}

async function init(dbname, table) {
    try {
        const db = await connect(dbname)
        console.log('Update table', table || '*')
        const m = model(db)
        const alter = true
        // update a table
        if (table) await m[table].sync({ alter })
        // update all
        else for (const i in m) await m[i].sync({ alter })
        console.log('Table updated', table || '*')
    } catch (e) {
        console.error(e)
    }
}

async function drop(dbname, table) {
    try {
        const db = await connect(dbname)
        console.log('Drop table', table || '*')
        const m = model(db)
        // drop table
        if (table) await m[table].drop()
        // drop all
        else for (const i in m) await m[i].drop()
        console.log('Table dropped', table || '*')
    } catch (e) {
        console.error(e)
    }
}

function model(db) {
    const tables = []
    for (const i in Model) tables[i] = db.define(Model[i].name, Model[i].table, Model[i].options)
    return tables
}

if (process.argv[1].includes('DB')) {
    if (process.argv[2] == 'test') connect(process.argv[3])
    if (process.argv[2] == 'init') init(process.argv[3], process.argv[4])
    if (process.argv[2] == 'drop') drop(process.argv[3], process.argv[4])
}

module.exports = { model, sqlite, mysql, connect }
