const $ = require('superagent')
const request = require('request')
const config = require('../config/network.json')
const puppeteer = require('puppeteer-extra')
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())
const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker')
puppeteer.use(AdblockerPlugin({ blockTrackers: true }))

const utils = {
    async get(url, query = {}) {
        console.log('request', url)
        console.log('query', query)
        const res = await $.get(url).query(query).timeout({
            response: 10000, // Wait 5 seconds for the server to start sending,
            deadline: 60000 // but allow 1 minute for the file to finish loading.
        })
        const html = res.text
        return html
    },
    async json(url, query = {}) {
        const res = await this.get(url, query)
        return JSON.parse(res)
    },
    /* get contract from scan API
    async getAPI(network, address) {
        const conf = config[network]
        const res = await utils.get(conf.api, {
            module: 'contract',
            action: 'getsourcecode',
            address,
            apikey: conf.apikey
        })
        const data = JSON.parse(res)
        if (data.status != '1') throw new Error('GET reponse status is not 1')
        const result = data.result
        if (result.length > 1) throw new Error('GET reponse result is not 1 length')
        return result[0]
    },
    */
    // superagent大陆访问出现-54 Error，临时更换request访问
    getAPI(network, address) {
        const conf = config[network]
        return new Promise((resolve, reject) => {
            request(
                {
                    url: `${conf.api}/?module=contract&action=getsourcecode&address=${address}&apikey=${conf.apikey}`,
                    timeout: 5000
                },
                (error, response, body) => {
                    if (!error && response.statusCode == 200) {
                        const data = JSON.parse(body)
                        if (data.status != '1') return reject('GET reponse status is not 1')
                        const result = data.result
                        if (result.length > 1) return reject('GET reponse result is not 1 length')
                        resolve(result[0])
                    } else reject(error)
                }
            )
        })
    },
    async sleep(time) {
        return new Promise(resolve => {
            setTimeout(resolve, time)
        })
    },
    // puppeteer在大陆也无法打开
    async getBrowser() {
        return await puppeteer.launch({ headless: false })
    },
    getTime(time) {
        if (!time) return new Date().getTime()
        else return new Date(time).getTime()
    },
    removeBlankLine(text) {
        return text.replace(/^\s*$(?:\r\n?|\n)/gm, '')
    },
    clearCode(code) {
        // remove commments
        code = code.replace(/(\/\*[\s\S]*?\*\/)|((?<!:)\/\/.*)/gm, '')
        // remove import
        code = code.replace(/import[^;]*;/gm, '')
        // remove pragma
        code = code.replace(/pragma[^;]*;/gm, '')
        // clear head and tail spaces
        return code.trim()
    },
    // support 10 layers { }
    getContracts(text) {
        text = this.hashStr(text)
        const reg = /(^|\s|)(contract|interface|library|abstract\s*contract)[^;{}]*{(?:[^{}]+|{(?:[^{}]+|{(?:[^{}]+|{(?:[^{}]+|{(?:[^{}]+|{(?:[^{}]+|{(?:[^{}]+|{(?:[^{}]+|{(?:[^{}]+|{[^{}]*})*})*})*})*})*})*})*})*})*}/gm
        const res = text.match(reg) || []
        for (const i in res) res[i] = this.hashStr(res[i].trim(), false)
        return res
    },
    // support 9 layers { }
    getFunctions(text) {
        text = this.hashStr(text)
        const reg = /(^|\s|)(function|event|modifier|constructor)[^{};]*({(?:[^}{]+|{(?:[^}{]+|{(?:[^}{]+|{(?:[^}{]+|{(?:[^}{]+|{(?:[^}{]+|{(?:[^}{]+|{(?:[^}{]+|{[^}{]*})*})*})*})*})*})*})*})*}|;)/gm
        const res = text.match(reg) || []
        for (const i in res) res[i] = this.hashStr(res[i].trim(), false)
        return res
    },
    // to mask string with '...', "..."
    hashStr(text, flag = true) {
        if (flag) {
            const reg = /("[^"]*")|('[^']*')/g
            text = text.replace(reg, s => `"${this.encode(s)}"`)
        } else {
            const reg = /"[^"]*"/g
            text = text.replace(reg, s => this.decode(s.slice(1).slice(0, -1)))
        }
        return text
    },
    encode(text) {
        return btoa(unescape(encodeURIComponent(text)))
    },
    decode(text) {
        return decodeURIComponent(escape(atob(text)))
    },
    // get first n words
    nWord(str, n) {
        if (typeof n === 'number') {
            const m = str.match(new RegExp('^(?:\\w+\\W+){' + n + '}(\\w+)'))
            return m && m[1]
        } else if (n.length) {
            const arr = []
            for (const i of n) arr.push(this.nWord(str, i))
            return arr
        }
    },
    getContractName(contractCode) {
        const words = this.nWord(contractCode, [0, 1, 2])
        if (words[0] === 'abstract') return words[2]
        else return words[1]
    },
    getFunctionName(functionCode) {
        const words = this.nWord(functionCode, [0, 1])
        if (words[0] === 'constructor') return words[0]
        else return words[1]
    },
    loadJson(path) {
        try {
            return require(path)
        } catch (e) {
            return []
        }
    }
}
module.exports = utils
