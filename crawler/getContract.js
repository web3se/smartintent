const { Contract } = require('./DB')
const T = Contract
async function count() {
    const res = await T.count()
    if (process.argv[2] == 'count') console.log(res)
    return res
}
async function maxId() {
    const res = await T.max('Id')
    if (process.argv[2] == 'max') console.log(res)
    return res
}
async function findOneByAddress(address, attributes) {
    const options = { where: { ContractAddress: address } }
    if (attributes) options.attributes = attributes
    const res = await T.findOne(options)
    if (process.argv[2] == 'get') console.log(res.dataValues)
    return res
}
async function findOneByPk(id, attributes) {
    const options = {}
    if (attributes) options.attributes = attributes
    const res = await T.findByPk(id, options)
    if (process.argv[2] == 'get') console.log(res.dataValues)
    return res
}
async function findNullContract() {
    const res = await T.findAll({ attributes: ['ContractAddress'], where: { SourceCode: '' } })
    const arr = []
    for (const item of res) arr.push(item.ContractAddress)
    if (process.argv[2] == 'findNull') console.log(arr)
    return arr
}

// check if exsited
async function check(address) {
    let flag = false
    const res = await T.count({
        where: {
            ContractAddress: address
        }
    })
    if (res > 0) flag = true
    console.log('Check Existed:', flag)
    return flag
}

async function insert(data) {
    const row = await T.create(data)
    console.log('Insert Contract', row.Id)
    console.log('Insert Contract', row.ContractAddress)
    return row
}
async function removeNull() {
    const row = await T.destroy({
        where: {
            SourceCode: ''
        }
    })
    return row
}

async function getNetworkById(id) {
    const res = await T.findByPk(id, { attributes: ['Network'] })
    console.log('Network', res.Network)
    return res.Network
}
async function getNetwork(ContractAddress) {
    const res = await T.findOne({
        where: { ContractAddress },
        attributes: ['Network']
    })
    console.log('Network', res.Network)
    return res.Network
}

const codeAttributes = [
    'Id',
    'ContractAddress',
    'SourceCode',
    'ContractName',
    'CompilerVersion',
    'Library',
    'EVMVersion',
    'Proxy',
    'ConstructorArguments',
    'OptimizationUsed',
    'Runs'
]

async function getCodeByAddress(ContractAddress) {
    const res = await T.findOne({
        where: { ContractAddress },
        attributes: codeAttributes
    })
    return res
}
async function getCodeById(id) {
    const res = await T.findByPk(id, {
        attributes: codeAttributes
    })
    return res
}
async function upsert(data) {
    const row = await T.upsert(data)
    console.log('Upsert Contract', row[0].ContractAddress)
    return row
}
async function updateByAddress(data) {
    const row = await T.update(data, { where: { ContractAddress: data.ContractAddress } })
    console.log('Update Contract', row[0].ContractAddress)
    return row
}

if (process.argv[1].includes('getContract')) {
    if (process.argv[2] == 'count') count()
    if (process.argv[2] == 'max') maxId()
    if (process.argv[2] == 'get') {
        if (process.argv[3].substring(0, 2) == '0x') findOneByAddress(process.argv[3])
        else findOneByPk(parseInt(process.argv[3]))
    }
    if (process.argv[2] == 'check') check(process.argv[3])
    if (process.argv[2] == 'findNull') findNullContract()
    if (process.argv[2] == 'removeNull') removeNull()
    if (process.argv[2] == 'network') {
        if (process.argv[3].substring(0, 2) == '0x') getNetwork(process.argv[3])
        else getNetworkById(process.argv[3])
    }
}

module.exports = {
    findOneByPk,
    findOneByAddress,
    findNullContract,
    count,
    maxId,
    check,
    insert,
    upsert,
    updateByAddress,
    removeNull,
    getNetworkById,
    getNetwork,
    getCodeByAddress,
    getCodeById
}
