const { Op } = require('sequelize')
const { Code } = require('./DB')
const T = Code
async function count() {
    const res = await T.count()
    if (process.argv[2] == 'count') console.log(res)
    return res
}
async function findOneByAddress(address, attributes) {
    const options = { where: { ContractAddress: address } }
    if (attributes) options.attributes = attributes
    const res = await T.findOne(options)
    if (process.argv[2] == 'get') console.log(res.dataValues)
    return res
}
async function findOneByPk(id, attributes) {
    const options = {}
    if (attributes) options.attributes = attributes
    const res = await T.findByPk(id, options)
    if (process.argv[2] == 'get') console.log(res.dataValues)
    return res
}

async function maxId() {
    const res = await T.max('id')
    if (process.argv[2] == 'max') console.log(res)
    return res
}
async function maxCompileId() {
    const res = await T.max('id', { where: { ABI: { [Op.not]: null } } })
    if (process.argv[2] == 'maxCompile') console.log(res)
    return res
}

// check if exsited
async function check(address) {
    let flag = false
    const res = await T.findAll({
        where: {
            ContractAddress: address
        }
    })
    if (res.length > 0) flag = true
    console.log('Check Existed:', flag)
    return flag
}

async function upsert(data) {
    const row = await T.upsert(data)
    return row
}
async function getSourceCodeMapById(id) {
    const attributes = ['Id', 'ContractAddress', 'SourceCodeMap']
    const res = await T.findByPk(id, { attributes })
    return res
}
async function getSourceCodeMapByAddress(ContractAddress) {
    const attributes = ['Id', 'ContractAddress', 'SourceCodeMap']
    const res = await T.findOne({ where: { ContractAddress }, attributes })
    return res
}
async function getSourceCodeById(id) {
    const res = await T.findByPk(id, { attributes: ['SourceCode'] })
    return res.SourceCode
}
async function getSourceCodeByAddress(ContractAddress) {
    const res = await T.findOne({
        where: { ContractAddress },
        attributes: ['SourceCode']
    })
    return res.SourceCode
}

if (process.argv[1].includes('getCode')) {
    if (process.argv[2] == 'count') count()
    if (process.argv[2] == 'get') {
        if (process.argv[3].substring(0, 2) == '0x') findOneByAddress(process.argv[3])
        else findOneByPk(parseInt(process.argv[3]))
    }
    if (process.argv[2] == 'check') check(process.argv[3])
    if (process.argv[2] == 'max') maxId()
    if (process.argv[2] == 'max-compile') maxCompileId()
}

module.exports = {
    findOneByPk,
    findOneByAddress,
    count,
    maxId,
    check,
    upsert,
    getSourceCodeById,
    getSourceCodeByAddress,
    maxCompileId,
    getSourceCodeMapById,
    getSourceCodeMapByAddress
}
