const xlsx = require('node-xlsx').default
const ROOT = require('app-root-path')
const FILE = `${ROOT}/db/contracts5.csv`
const { findOneByPk, maxId, check, insert, removeNull } = require('./getContract')
const { upsert } = require('./getToken')
const { sleep, get, getAPI } = require('./utils')
const cheerio = require('cheerio')
const NETWORK = 'bscMain'
const config = require('../config/network.json')[NETWORK]

// /db/csv files
async function getFromCSV(from) {
    try {
        console.log('Get from CSV', FILE)
        const data = xlsx.parse(FILE)[0].data
        data.splice(0, 2)
        const addrs = []
        let start = false
        for (const item of data) {
            if (item[1] === from || !from) start = true
            if (!start) continue
            addrs.push(item[1])
        }
        console.log('Start from', from)
        for (const item of addrs) {
            from = item
            const flag = await check(item)
            if (flag) continue // skip existed contract
            if (item.substring(0, 2) !== '0x') continue // slip invalid contract address
            const source = await getAPI(NETWORK, item)
            source.ContractAddress = item
            source.Network = NETWORK
            if (source.SourceCode) await insert(source)
        }
    } catch (e) {
        console.error(e)
        getFromCSV(from)
    }
}

// give address and download contract source code
async function getFromAddress(address) {
    try {
        console.log('Get from Address', address)
        const flag = await check(address)
        if (!flag) {
            const source = await getAPI(NETWORK, address)
            source.ContractAddress = address
            source.Network = NETWORK
            if (source.SourceCode) await insert(source)
        }
    } catch (e) {
        console.error(e)
    }
}

// download a token list from https://bscscan.com/tokens
async function getFromTokens() {
    try {
        const url = config['tokens']
        console.log('Get from Token List', url)
        for (let i = 1; i <= 19; i++) {
            const html = await get(url + '?p=' + i)
            const h = cheerio.load(html)
            const a = h('#tblResult a.text-primary')
            for (const e of a) {
                const href = h(e).attr('href').split('/')
                const addr = href[2]
                const flag = await check(addr)
                if (flag) continue // skip existed contract
                const source = await getAPI(NETWORK, addr)
                source.ContractAddress = addr
                source.Network = NETWORK
                if (source.SourceCode) await insert(source)
            }
        }
    } catch (e) {
        console.error(e)
    }
}

// add Creator, TxHash, ContractType=isToken?
async function labelToken(start = 1, end) {
    try {
        end = parseInt(end) || (await maxId())
        let i = parseInt(start)
        while (i <= end) {
            const data = await findOneByPk(i)
            if (!data) continue
            if (!data.SourceCode) continue // skip not verfied contract
            if (data.Network != NETWORK) continue
            if (data.ContractType) await upsert({ ContractAddress: data.ContractAddress })
            if (!data.Creator || !data.TxHash) {
                // update info, label is token?
                const url = `${config.address}/${data.ContractAddress}`
                const html = await get(url)
                const h = cheerio.load(html)
                const creator = h('#ContentPlaceHolder1_trContract .hash-tag[title="Creator Address"]').text()
                const txHash = h('#ContentPlaceHolder1_trContract .hash-tag[title="Creator Txn Hash"]').text()
                if (!creator || !txHash) {
                    console.log('sleep', 10000)
                    await sleep(10000)
                    continue
                }
                if (creator) data.Creator = creator
                if (txHash) data.TxHash = txHash
                // is token?
                const tokenH = h('#ContentPlaceHolder1_tr_tokeninfo')
                if (tokenH.length > 0) data.ContractType = tokenH.find('.row>.col-md-8>a').text().trim()
                if (data.ContractType) await upsert({ ContractAddress: data.ContractAddress })
                const res = await data.save()
                console.log('Update Info', res.Id)
            }
            i++
        }
    } catch (e) {
        console.error(e)
    }
}

if (process.argv[1].includes('updateContract')) {
    // argv 2 is contract address
    if (process.argv[2].substring(0, 2) == '0x') getFromAddress(process.argv[2])
    else if (process.argv[2] == 'tokens') getFromTokens()
    else if (process.argv[2] == 'csv') getFromCSV(process.argv[3])
    else if (process.argv[2] == 'labelToken') labelToken(process.argv[3], process.argv[4])
    else if (process.argv[2] == 'removeNull') removeNull()
}
