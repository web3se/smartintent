const { model, sqlite, mysql } = require('./initDB')
const conf = require('../config/mysql.json')
module.exports = model(mysql(conf))
