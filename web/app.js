const express = require('express')
const app = express()
const CTRL = require('app-root-path') + '/web/'

function router() {
    const allowCrossDomain = function (_, res, next) {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Methods', 'GET,POST')
        res.header('Access-Control-Allow-Headers', 'Content-Type')
        res.header('Access-Control-Allow-Credentials', 'true')
        next()
    }
    app.use(allowCrossDomain)
    app.get('/', (_, res) => {
        res.send('<h1>Web3 Crack</h1>')
    })
    app.all('*', (req, res, next) => {
        const path = req.path.toString()
        const ctl = path.split('/')[1]
        const act = path.split('/')[2]
        try {
            const fun = require(CTRL + ctl)
            fun[act](req, res, next)
        } catch (e) {
            res.send('404')
        }
    })
}
router()

app.listen(8080)
