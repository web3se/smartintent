const { findOneByPk, findOneByAddress, count, maxId } = require('../crawler/getCode')

async function get(req, res, next) {
    try {
        const keyword = req.query.key
        let data
        const attrs = ['Id', 'ContractAddress', 'SourceCode', 'SourceCodeMap', 'OpCode', 'MethodIdentifiers', 'ABI']
        if (!keyword) data = { maxId: await maxId(), count: await count() }
        else if (keyword.substr(0, 2) === '0x') data = await findOneByAddress(keyword, attrs)
        else data = await findOneByPk(keyword, attrs)
        res.json(data)
    } catch (e) {
        next(e)
    }
}

module.exports = { get }
