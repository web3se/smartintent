const $data = require('../crawler/getData')
const highlight = require('../tensorflow/highlight')
const ROOT = require('app-root-path')
const LOG = `${ROOT}/tensorflow/logs`
const $ = require('../crawler/utils')

async function sourceCodeRisk(req, res, next) {
    try {
        const key = req.query.key // key is for token table
        const data = await $data.getSourceCodeScam(key)
        if (!data) return res.sendStatus(404)
        const risk = JSON.parse(data.Scams)
        const sourceCode = JSON.parse(data.code.SourceCodeMap)
        const address = data.ContractAddress
        return res.json({ address, risk, sourceCode })
    } catch (e) {
        next(e)
    }
}

async function highPredict(req, res, next) {
    try {
        const key = req.query.key
        const { funObj, k } = await highlight.predict(key)
        return res.json({ k: k, data: funObj })
    } catch (e) {
        next(e)
    }
}

// return evaluation data
async function evaluate(_, res, next) {
    try {
        const data = [
            {
                title: 'SiLSTM',
                data: $.loadJson(`${LOG}/mymodel_lstm/evaluate-32.json`)
            },
            {
                title: 'BiLSTM',
                data: $.loadJson(`${LOG}/mymodel_bilstm/evaluate-32.json`)
            },
            {
                title: 'SiLSTM Highlight ASC',
                data: $.loadJson(`${LOG}/mymodel_lstm_high_asc/evaluate-32.json`)
            },
            {
                title: 'BiLSTM Highlight ASC',
                data: $.loadJson(`${LOG}/mymodel_bilstm_high_asc/evaluate-32.json`)
            },
            {
                title: 'SiLSTM Highlight Scale X2',
                data: $.loadJson(`${LOG}/mymodel_lstm_high_scale/evaluate-32X2.json`)
            },
            {
                title: 'BiLSTM Highlight Scale X2',
                data: $.loadJson(`${LOG}/mymodel_bilstm_high_scale/evaluate-32X2.json`)
            },
            {
                title: 'BiLSTM Highlight Scale X2 Dropout',
                data: $.loadJson(`${LOG}/mymodel_bilstm_high_scale/evaluate-32X2-dropout.json`)
            },
            {
                title: 'BiLSTM Highlight Scale X4',
                data: $.loadJson(`${LOG}/mymodel_bilstm_high_scale/evaluate-32X4.json`)
            },
            {
                title: 'BiLSTM Highlight Scale X10',
                data: $.loadJson(`${LOG}/mymodel_bilstm_high_scale/evaluate-32X10.json`)
            },
            /*
            {
                title: 'LSTM Word Embedding (48 X 16)',
                data: $.loadJson(`${LOG}/mymodel_lstm_word/evaluate-48X16.json`)
            },
            {
                title: 'LSTM Word Embedding (128 X 4)',
                data: $.loadJson(`${LOG}/mymodel_lstm_word/evaluate-128X4.json`)
            },
            */
            {
                title: 'CNN',
                data: $.loadJson(`${LOG}/mymodel_cnn/evaluate.json`)
            }
        ]
        return res.json(data)
    } catch (e) {
        next(e)
    }
}

module.exports = { sourceCodeRisk, highPredict, evaluate }
