const { findOneByPk, findOneByAddress, count, maxId } = require('../crawler/getContract')

async function get(req, res, next) {
    try {
        const keyword = req.query.keyword
        let data
        if (keyword === 'count') data = await count()
        else if (keyword.substr(0, 2) === '0x') data = await findOneByAddress(keyword)
        else if (!keyword) data = await maxId()
        else data = await findOneByPk(keyword)
        res.json(data)
    } catch (e) {
        next(e)
    }
}

module.exports = { get }
