/*
 * BiLSTM + intent highlight slice, average
 */

const MyModel = require('./mymodel')
const { load } = require('./highlight')
const kmeans = load()

class BiLSTMHighSlice extends MyModel {
    buildModel() {
        const tf = this.tf
        const mask = tf.layers.masking({ maskValue: this.MASK, inputShape: [this.FUNS, this.INPUT] })
        const lstm = tf.layers.bidirectional({
            layer: tf.layers.lstm({ units: this.UNIT, returnSequences: false })
        })
        const sigmoid = tf.layers.dense({ units: Object.keys(this.TYPE).length, activation: 'sigmoid' })
        return tf.sequential({ layers: [mask, lstm, sigmoid] })
    }
    // slice low highlight
    padding(xs) {
        xs = xs.map(x => {
            // order by distance desc
            x = x.sort((x1, x2) => {
                return this.tf.tidy(() => {
                    const d1 = kmeans.predict(this.tf.tensor(x1)).distance.arraySync()[0]
                    const d2 = kmeans.predict(this.tf.tensor(x2)).distance.arraySync()[0]
                    return d2 - d1
                })
            })
            /*
            console.log('check===========')
            x.map(b =>
                console.log(
                    kmeans.predict(tf.tensor(b)).index.arraySync()[0],
                    kmeans.predict(tf.tensor(b)).distance.arraySync()[0]
                )
            )
            */
            // padding MASK
            while (x.length < this.FUNS) x.push(new Array(this.INPUT).fill(this.MASK))
            // average > FUNS
            if (x.length > this.FUNS) {
                //console.log('average===================================================')
                const add = x.slice(this.FUNS - 1)
                const avg = this.tf.tidy(() => {
                    let fun = this.tf.zeros([this.INPUT])
                    for (const i in add) fun = fun.add(this.tf.tensor(add[i]))
                    return fun.div(this.tf.scalar(add.length)).arraySync()
                })
                x = x.slice(0, this.FUNS - 1)
                x.push(avg)
                console.log(x.length)
                //console.log('average===================================================\n')
            }
            return x
        })
        return xs
    }
}

const nn = new BiLSTMHighSlice('mymodel_bilstm_high_slice')

if (process.argv[2] == 'train') nn.train(process.argv[3], process.argv[4], process.argv[5], process.argv[6])
if (process.argv[2] == 'evaluate') nn.evaluate(process.argv[3], process.argv[4], process.argv[5])
if (process.argv[2] == 'predict') nn.predict(process.argv[3], process.argv[4])
if (process.argv[2] == 'summary') nn.summary()
