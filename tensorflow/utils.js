const TYPE = require('./type')
const tf = require('@tensorflow/tfjs-node-gpu')
const ROOT = require('app-root-path')
const fs = require('fs')
const { BertWordPieceTokenizer } = require('@nlpjs/bert-tokenizer')
const vocab = fs.readFileSync(`${ROOT}/tensorflow/models/solidity-tokenizer/vocab.txt`, 'utf-8')
const $t = new BertWordPieceTokenizer({ vocabContent: vocab })

module.exports = {
    UNIT: 32, // LSTM hidden units
    MASK: 0.0, // pad mask
    TYPE: TYPE, // 10 type dict
    DIST: 0.2, // highlight within distance bound
    MULT: 2, // highlight scale size
    INPUT: 512, // input size of the first NN layer, the lowest dim
    FUNS: 128, // slice functions
    tf: tf,
    tokenize(s) {
        return $t.tokenizeSentence(s).ids
    }
}
