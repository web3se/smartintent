const MyModel = require('./mymodel')
const { load } = require('./highlight')
const kmeans = load()

class LSTMHighScale extends MyModel {
    // build my model structure
    buildModel() {
        const tf = this.tf
        const mask = tf.layers.masking({ maskValue: this.MASK, inputShape: [null, 512] })
        const lstm = tf.layers.lstm({ units: this.UNIT, returnSequences: false })
        const sigmoid = tf.layers.dense({ units: Object.keys(this.TYPE).length, activation: 'sigmoid' })
        return tf.sequential({ layers: [mask, lstm, sigmoid] })
    }

    // scale highlight
    padding(xs) {
        // rank by distance desc
        console.log('==========================highlight scale padding==========================')
        console.log('Scaling...', this.MULT)
        xs = xs.map(x =>
            x.map(v =>
                kmeans.predict(this.tf.tensor(v)).distance.arraySync()[0] < this.DIST
                    ? v
                    : this.tf.tensor(v).mul(this.tf.scalar(this.MULT)).arraySync()
            )
        )
        // padding
        console.log('Finding max length...')
        const maxLength = Math.max.apply(
            Math,
            xs.map(x => x.length)
        )
        console.log('Padding...', maxLength)
        return xs.map(x => {
            while (x.length < maxLength) x.push(new Array(512).fill(this.MASK))
            return x
        })
    }
}

const nn = new LSTMHighScale('mymodel_lstm_high_scale')

if (process.argv[2] == 'train') nn.train(process.argv[3], process.argv[4], process.argv[5], process.argv[6])
if (process.argv[2] == 'evaluate') nn.evaluate(process.argv[3], process.argv[4], process.argv[5])
if (process.argv[2] == 'predict') nn.predict(process.argv[3], process.argv[4])
if (process.argv[2] == 'summary') nn.summary()
