/*
 * BiLSTM model highlight rank desc
 * use a bidirectional LSTM layer
 */

const MyModel = require('./mymodel')
const { load } = require('./highlight')
const kmeans = load()

class BiLSTMHighDESC extends MyModel {
    // build my model structure
    buildModel() {
        const tf = this.tf
        const mask = tf.layers.masking({ maskValue: this.MASK, inputShape: [null, 512] })
        const lstm = tf.layers.bidirectional({
            layer: tf.layers.lstm({ units: this.UNIT, returnSequences: false })
        })
        const sigmoid = tf.layers.dense({ units: Object.keys(this.TYPE).length, activation: 'sigmoid' })
        return tf.sequential({ layers: [mask, lstm, sigmoid] })
    }
    // scale highlight
    padding(xs) {
        // rank by distance desc
        console.log('==========================highlight rank padding==========================')
        console.log('rank...desc')
        xs = xs.map(x =>
            x.sort(
                (x1, x2) =>
                    kmeans.predict(this.tf.tensor(x2)).distance.arraySync()[0] -
                    kmeans.predict(this.tf.tensor(x1)).distance.arraySync()[0]
            )
        )
        // padding
        const maxLength = Math.max.apply(
            Math,
            xs.map(x => x.length)
        )
        console.log('padding...')
        return xs.map(x => {
            while (x.length < maxLength) x.push(new Array(512).fill(this.MASK))
            return x
        })
    }
}

const nn = new BiLSTMHighDESC('mymodel_bilstm_high_desc')

if (process.argv[2] == 'train') nn.train(process.argv[3], process.argv[4], process.argv[5], process.argv[6])
if (process.argv[2] == 'evaluate') nn.evaluate(process.argv[3], process.argv[4], process.argv[5])
if (process.argv[2] == 'predict') nn.predict(process.argv[3], process.argv[4])
if (process.argv[2] == 'summary') nn.summary()
