const MyModel = require('./mymodel')
const $ = require('./utils')

const TOKEN = 32 // vector size for one word token
const INPUT = 128 // input vector size (one function is 128 * 4 = 512)
const VOCAB = 52000 + 1 // total vocabularies
const FUNS = 256

class CNNWord extends MyModel {
    constructor(name) {
        super(name)
        this.INPUT = INPUT
        this.VOCAB = VOCAB
        this.TOKEN = TOKEN
        this.FUNS = FUNS
        this.encoder = true
    }

    // build my model structure
    buildModel() {
        const tf = this.tf
        const embed = tf.layers.embedding({
            name: 'Embed',
            inputDim: this.VOCAB,
            outputDim: this.TOKEN,
            inputShape: [this.FUNS, this.INPUT]
        })
        const conv1 = tf.layers.conv2d({
            name: 'Conv_1',
            kernelSize: 8,
            filters: 64,
            activation: 'relu'
        })
        const maxPool1 = tf.layers.maxPooling2d({ name: 'Max_pool_1', poolSize: [3, 3] })
        const conv2 = tf.layers.conv2d({
            name: 'Conv_2',
            kernelSize: 8,
            filters: 512,
            activation: 'relu'
        })
        const maxPool2 = tf.layers.averagePooling2d({ name: 'Avg_pool_2', poolSize: [3, 3] })
        const flatten = tf.layers.flatten()
        const sigmoid = tf.layers.dense({
            name: 'Sigmoid',
            units: Object.keys(this.TYPE).length,
            activation: 'sigmoid'
        })
        return tf.sequential({ layers: [embed, conv1, maxPool1, conv2, maxPool2, flatten, sigmoid] })
    }

    // rewrite preX
    preX(xs) {
        const ws = []
        for (const i in xs) {
            for (const j in xs[i]) {
                // one function
                const ids = $.tokenize(xs[i][j])
                while (ids.length < this.INPUT) ids.push(this.MASK)
                ws.push(ids.slice(0, this.INPUT)) // intercept first 128 word tokens (512 input size)
            }
        }
        return ws // [*, 128]
    }

    // scale highlight
    padding(xs) {
        console.log('==========================CNN constant padding==========================')
        console.log('Padding...', this.FUNS)
        return xs.map(x => {
            console.log('Origin length', x.length)
            while (x.length < this.FUNS) x.push(Array(this.INPUT).fill(this.MASK))
            console.log('Padded length-------->', x.length)
            return x.slice(0, this.FUNS)
        }) // return a matrix [batchSize, words, wordSize]
    }
}

const nn = new CNNWord('mymodel_cnn_word')

if (process.argv[2] == 'train') nn.train(process.argv[3], process.argv[4], process.argv[5], process.argv[6])
if (process.argv[2] == 'evaluate') nn.evaluate(process.argv[3], process.argv[4], process.argv[5])
if (process.argv[2] == 'predict') nn.predict(process.argv[3], process.argv[4])
if (process.argv[2] == 'summary') nn.summary()
