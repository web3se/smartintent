const TreebankTokenizer = require('treebank-tokenizer')
const $code = require('../crawler/getCode')
const $word = require('../crawler/getWord')

let MAX = 0
let MAX_ID = 0
let MAX_CON = ''
let MAX_FUN = ''

async function getTokensMax(start = 1) {
    const max = await $code.maxId()
    for (let i = start; i <= max; i++) await getTokens(i)
    console.log('MAX', MAX)
    console.log('ID', MAX_ID)
    console.log('Contract', MAX_CON)
    console.log('Function', MAX_FUN)
}
async function getFuncMax(start = 1) {
    const max = await $code.maxId()
    for (let i = start; i <= max; i++) {
        const length = await countFuncs(i)
        setMax(length, i)
    }
    console.log('Max', MAX)
    console.log('ID', MAX_ID)
}
async function countFuncs(id) {
    let data = await $code.getEmbeddingById(id)
    if (!data) return 0
    data = JSON.parse(data.Embedding)
    let length = 0
    for (const j in data)
        for (const k in data[j]) {
            console.log(k)
            length++
        }
    console.log(length)
    return length
}

async function getTokens(key) {
    const t = new TreebankTokenizer()
    let code = null
    if (typeof key === 'string' && key.substring(0, 2) === '0x') code = await $code.getSourceCodeMapByAddress(key)
    else code = await $code.getSourceCodeMapById(key)
    if (!code) return code

    // tokenize
    const codeMap = JSON.parse(code.SourceCodeMap)
    for (const i in codeMap) {
        for (const j in codeMap[i]) {
            codeMap[i][j] = t.tokenize(codeMap[i][j])
            setMax(codeMap[i][j].length, key, i, j)
        }
    }

    if (process.argv[2] === 'get') {
        console.log(codeMap)
        console.log('Key (Id/Address)', MAX_ID)
        console.log('Max tokens', MAX)
        console.log('Contract', MAX_CON)
        console.log('Function', MAX_FUN)
    }

    return codeMap
}

function setMax(num, id, contract, func) {
    if (num > MAX) {
        MAX = num
        MAX_ID = id
        MAX_CON = contract
        MAX_FUN = func
    }
}

async function updateWordAll(start = 1) {
    try {
        const max = await $code.maxId()
        for (let i = start; i <= max; i++) {
            const words = await getTokens(i)
            if (!words) continue
            console.log('Code Id', i)
            const data = new Set()
            for (const j in words) for (const k in words[j]) for (const item of words[j][k]) data.add(item)
            const res = await $word.update(data)
            console.log('update', res.length)
        }
    } catch (e) {
        console.error(e.message)
    }
}

if (process.argv[1].includes('tokenizer')) {
    if (process.argv[2] === 'max') getTokensMax(process.argv[3])
    if (process.argv[2] === 'update-word') updateWordAll(process.argv[3])
    if (process.argv[2] === 'max-fun') getFuncMax(process.argv[3])
    if (process.argv[2] === 'count-fun') countFuncs(process.argv[3])
}

module.exports = {
    getTokens
}
