/*
 * Use Convolutional neural network
 */

const MyModel = require('./mymodel')
const FUNS = 256

class CNN extends MyModel {
    constructor(name) {
        super(name)
        this.FUNS = FUNS
    }

    buildModel() {
        const tf = this.tf
        const conv1 = tf.layers.conv1d({
            inputShape: [this.FUNS, this.INPUT],
            name: 'Conv_1',
            kernelSize: 32,
            filters: 128,
            activation: 'relu'
        })
        const pool1 = tf.layers.maxPooling1d({ name: 'Max_pool_1', poolSize: 4 })
        const conv2 = tf.layers.conv1d({
            name: 'Conv_2',
            kernelSize: 8,
            filters: 64,
            activation: 'relu'
        })
        const pool2 = tf.layers.averagePooling1d({ name: 'Avg_pool_2', poolSize: 4 })
        const flatten = tf.layers.flatten()
        const sigmoid = tf.layers.dense({
            name: 'Sigmoid',
            units: Object.keys(this.TYPE).length,
            activation: 'sigmoid'
        })
        return tf.sequential({ layers: [conv1, pool1, conv2, pool2, flatten, sigmoid] })
    }
    // scale highlight
    padding(xs) {
        console.log('==========================CNN constant padding==========================')
        console.log('Padding...', this.FUNS)
        return xs.map(x => {
            console.log('Origin length', x.length)
            while (x.length < this.FUNS) x.push(Array(this.INPUT).fill(this.MASK))
            console.log('Padded length-------->', x.length)
            return x.slice(0, this.FUNS)
        }) // return a matrix [batchSize, words, wordSize]
    }
}

const nn = new CNN('mymodel_cnn')

if (process.argv[2] == 'train') nn.train(process.argv[3], process.argv[4], process.argv[5], process.argv[6])
if (process.argv[2] == 'evaluate') nn.evaluate(process.argv[3], process.argv[4], process.argv[5])
if (process.argv[2] == 'predict') nn.predict(process.argv[3], process.argv[4])
if (process.argv[2] == 'summary') nn.summary()
