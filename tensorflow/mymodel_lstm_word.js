const MyModel = require('./mymodel')
const $ = require('./utils')

const TOKEN = 16 // one word vector size
const INPUT = 48 // input words
const VOCAB = 52000 + 1 // total vocabularies

class LSTMWord extends MyModel {
    constructor(name) {
        super(name)
        this.INPUT = INPUT
        this.VOCAB = VOCAB
        this.TOKEN = TOKEN
        this.encoder = true
    }

    // build my model structure
    buildModel() {
        const tf = this.tf
        const mask = tf.layers.masking({ name: 'Mask_input', maskValue: this.MASK, inputShape: [null, this.INPUT] })
        const embed = tf.layers.embedding({ name: 'Embed', inputDim: this.VOCAB, outputDim: this.TOKEN })
        const reshape = tf.layers.reshape({ name: 'Reshape', targetShape: [null, this.INPUT * this.TOKEN] })
        const drop = tf.layers.dropout({ name: 'Dropout', rate: 0.5 })
        const lstm = tf.layers.lstm({ name: 'LSTM', units: this.UNIT, returnSequences: false })
        const sigmoid = tf.layers.dense({
            name: 'Sigmoid',
            units: Object.keys(this.TYPE).length,
            activation: 'sigmoid'
        })
        return tf.sequential({ layers: [mask, embed, reshape, drop, lstm, sigmoid] })
    }

    // rewrite preX
    preX(xs) {
        const ws = []
        for (const i in xs) {
            for (const j in xs[i]) {
                // one function
                const ids = $.tokenize(xs[i][j])
                while (ids.length < this.INPUT) ids.push(this.MASK)
                ws.push(ids.slice(0, this.INPUT)) // intercept first 128 word tokens (512 input size)
            }
        }
        return ws // [*, 128]
    }
}

const nn = new LSTMWord('mymodel_lstm_word')

if (process.argv[2] == 'train') nn.train(process.argv[3], process.argv[4], process.argv[5], process.argv[6])
if (process.argv[2] == 'evaluate') nn.evaluate(process.argv[3], process.argv[4], process.argv[5])
if (process.argv[2] == 'predict') nn.predict(process.argv[3], process.argv[4])
if (process.argv[2] == 'summary') nn.summary()
